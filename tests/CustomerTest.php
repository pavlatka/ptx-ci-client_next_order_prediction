<?php

/**
 * Class CustomerTest
 */
class CustomerTest extends PHPUnit_Framework_TestCase {

    /**
     * @var
     */
    private $Customer;

    /**
     * Prepare $this->Customer before each test.
     */
    public function setUp()
    {
        $this->Customer = new Customer();
    }

    /**
     * @param int   $customerId
     * @param array $expected
     * @dataProvider data_testGetPurchaseHistory()
     */
    public function testGetPurchaseHistory_CustomerId_CorrectHistoryArray($customerId, $expected)
    {
        $result = $this->Customer->getPurchaseHistory($customerId);
        $this->assertEquals($result, $expected, 'Customer: ' . $customerId);
    }

    /**
     * @param int   $customerId
     * @param array $expected
     * @dataProvider data_testPredictNextPurchase
     */
    public function testPredictNextPurchase_CustomerId_CorrectPrediction($customerId, $expected)
    {
        $prediction = $this->Customer->predictNextPurchase($customerId);
        $this->assertEquals($prediction, $expected, 'Customer: ' . $customerId);
    }

    /**
     * Data generator.
     *
     * @return array
     */
    public function data_testGetPurchaseHistory()
    {
        return array(
            array(1, array(
                '2015-04-01' => array(
                    [1, 2, -2.00],
                    [1, 2, -3.00])
                )
            ),
            array(2, array(
                '2014-10-01' => array(
                    [3, 2, -2.00],
                    [3, 2, -3.00]),
                '2015-01-01' => array(
                    [3, 2, -1.50],
                    [3, 2, -3.50]),
                '2015-04-15' => array(
                    [3, 2, -1.50],
                    [3, 2, -3.50])
                )
            ),
            array(3, array(
                '2014-08-01' => array(
                    [3, 4, -4.00],
                    [3, 4, -5.00],
                    [2, 2, +0.50]),
                )
            ),
            array(4, array(
                '2014-01-01' => array(
                    [3, 2, -4.00],
                ),
                '2014-02-02' => array(
                    [3, 2, -4.00],
                )
            )),
            array(5, array())
        );
    }

    /**
     * Data generator.
     *
     * @return array
     */
    public function data_testPredictNextPurchase()
    {
        return array(
            array(1, array(
                '2016-03-26' => array(
                    1 => array(-2, -3)
                ),
            )),
            array(2, array(
                '2014-11-30' => array(
                    3 => array(-2, -3)
                ),
                '2015-06-14' => array(
                    3 => array(-1.5, -3.5)
                )
            )),
            array(3, array(
                '2015-01-28' => array(
                    2 => array(0.5)
                ),
                '2014-11-29' => array(
                    3 => array(-4, -5)
                )
            )),
            array(4, array(
                '2014-05-01' => array(
                    3 => array(-4)
                ),
            )),
            array(5, array())
        );
    }


    /**
     * Just unset $this->Customer after each test.
     */
    public function tearDown()
    {
        unset($this->Customer);
    }
}