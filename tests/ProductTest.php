<?php

/**
 * Class ProductTest
 */
class ProductTest extends PHPUnit_Framework_TestCase {

    /**
     * @var
     */
    private $Product;

    /**
     * Prepare $this->Product before each test.
     */
    public function setUp() {
        $this->Product = new Product();
    }

    /**
     *
     * @param int $productId
     * @param int $expected
     * @dataProvider data_testgetExpiration()
     */
    public function testgetExpiration_ProductId_CorrectExpiration($productId, $expected)
    {
        $result = $this->Product->getExpiration($productId);
        $this->assertEquals($result, $expected);
    }

    /**
     * Generator function.
     * @return array
     */
    public function data_testgetExpiration() {
        return [
            [1, 180],
            [2, 90],
            [3, 30],
            [4, -1] // Just to test result for unknown product.
        ];
    }

    /**
     * Just unset $this->Product after each test.
     */
    public function tearDown()
    {
        unset($this->Product);
    }
}