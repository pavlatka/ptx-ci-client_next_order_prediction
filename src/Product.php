<?php
/**
 * Product
 *
 * PHP version 5.4
 *
 * @category Interview_Test
 * @package  VaseCocky
 * @author   Tomas Pavlatka <tomas@pavlatka.cz>
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons License
 * @link     http://www.pavlatka.cz
 */

/**
 * Class Product
 *
 * @category Interview_Test
 * @package  VaseCocky
 * @author   Tomas Pavlatka <tomas@pavlatka.cz>
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons License
 * @link     http://www.pavlatka.cz
 */
class Product
{

    /**
     * Constructor.
     */
    public function __construct()
    {

    }

    /**
     * Returns expiration for a product.
     *
     * @param int $productId - id of the product
     *
     * @return int | -1     
     */
    public function getExpiration($productId)
    {
        // TODO: This is just for a test. Data will be pulled from db in real world.
        $goods = [
            1 => 180,
            2 => 90,
            3 => 30
        ];

        if (array_key_exists($productId, $goods)) {
            return (int)$goods[$productId];
        }

        return -1;
    }
}