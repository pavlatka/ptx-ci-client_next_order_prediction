<?php
/**
 * Customer
 *
 * PHP version 5.4
 *
 * @category Interview_Test
 * @package  VaseCocky
 * @author   Tomas Pavlatka <tomas@pavlatka.cz>
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons License
 * @link     http://www.pavlatka.cz
 */

/**
 * Class Customer
 *
 * @category Interview_Test
 * @package  VaseCocky
 * @author   Tomas Pavlatka <tomas@pavlatka.cz>
 * @license  http://creativecommons.org/licenses/by/4.0/ Creative Commons License
 * @link     http://www.pavlatka.cz
 */
class Customer
{
    /**
     * Returns purchase history for a customer
     *
     * @param int $customerId - id of a customer
     *
     * @return array
     */
    public function getPurchaseHistory($customerId)
    {
        // TODO: This is just for a test. Data will be pulled from db in real world.
        $purchasesData = array(
            1 => array(
                '2015-04-01' => array(
                    [1, 2, -2.00],
                    [1, 2, -3.00]
                )
            ),
            2 => array(
                '2014-10-01' => array(
                    [3, 2, -2.00],
                    [3, 2, -3.00]
                ),
                '2015-01-01' => array(
                    [3, 2, -1.50],
                    [3, 2, -3.50]
                ),
                '2015-04-15' => array(
                    [3, 2, -1.50],
                    [3, 2, -3.50]
                )
            ),
            3 => array(
                '2014-08-01' => array(
                    [3, 4, -4.00],
                    [3, 4, -5.00],
                    [2, 2, +0.50]
                ),
            ),
            4 => array(
                '2014-01-01' => array(
                    [3, 2, -4.00],
                ),
                '2014-02-02' => array(
                    [3, 2, -4.00],
                ),
            )
        );

        if (array_key_exists($customerId, $purchasesData)) {
            return $purchasesData[$customerId];
        }

        return array();
    }

    /**
     * Predicts next purchase for a customer from
     * his purchase history.
     *
     * @param int $customerId - id of a customer
     *
     * @return array
     */
    public function predictNextPurchase($customerId)
    {
        $purchaseHistory = $this->getPurchaseHistory($customerId);

        if (!empty($purchaseHistory)) {
            // Create prediction.
            $predictions = [];
            $predictionsInfo = $this->getPurchasePredictionInfo($purchaseHistory);
            foreach ($predictionsInfo as $productId => $dioptersData) {
                foreach ($dioptersData as $diopter => $predictionDate) {
                    if (!array_key_exists($predictionDate, $predictions)) {
                        $predictions[$predictionDate] = [
                            $productId => [
                                $diopter,
                            ]
                        ];
                    } else if (!array_key_exists($productId, $predictions[$predictionDate])) {
                        $predictions[$predictionDate][$productId] = [$diopter];
                    } else {
                        $predictions[$predictionDate][$productId][] = $diopter;
                    }
                }
            }

            return $predictions;
        }

        return array();
    }

    /**
     * Groups products their diopter.
     *
     * @param array $purchaseHistory - array with purchases history
     *
     * @return array
     */
    protected function createPurchaseProductGroups($purchaseHistory)
    {
        if (!empty($purchaseHistory)) {
            $productGroups = [];
            foreach ($purchaseHistory as $date => $purchaseData) {
                foreach ($purchaseData as $itemInfo) {
                    list($productId, $quantity, $diopter) = $itemInfo;
                    $diopter = (string) $diopter;

                    if (!array_key_exists($productId, $productGroups)) {
                        $productGroups[$productId] = [
                            $diopter => [
                                'dates' => [
                                    $date => $quantity
                                ],
                            ]
                        ];
                    } else {
                        if (!array_key_exists($diopter, $productGroups[$productId])) {
                            $productGroups[$productId][$diopter] = [
                                'dates' => [
                                    $date => $quantity
                                ]
                            ];
                        } else {
                            $productGroups[$productId][$diopter]['dates'][$date] = $quantity;
                        }
                    }
                }
            }

            return $productGroups;
        }

        return array();
    }

    /**
     * Returns prediction information.
     *
     * @param array $purchaseHistory - array with purchases history.
     *
     * @return array
     */
    protected function getPurchasePredictionInfo($purchaseHistory)
    {
        $productGroups = $this->createPurchaseProductGroups($purchaseHistory);

        if (!empty($productGroups)) {
            $predictionsInfo = [];
            $productObj = new Product();
            foreach ($productGroups as $productId => $purchaseHistory) {
                $productExpiration = $productObj->getExpiration($productId);

                foreach ($purchaseHistory as $diopter => $predictionsData) {
                    foreach ($predictionsData['dates'] as $date => $quantity) {

                        $diopter = (string) $diopter;
                        $extendPeriod = $quantity * $productExpiration;

                        if (!array_key_exists($productId, $predictionsInfo)) {
                            $newDateTime = strtotime($date . '+' . $extendPeriod . 'days');
                            $predictionsInfo[$productId] = [
                                $diopter => date('Y-m-d', $newDateTime)
                            ];
                        } else {
                            if (!array_key_exists($diopter, $predictionsInfo[$productId])) {

                                $newDateTime = strtotime($date . '+' . $extendPeriod . 'days');
                                $predictionsInfo[$productId][$diopter] = date('Y-m-d', $newDateTime);
                            } else {
                                $predictionDate = $predictionsInfo[$productId][$diopter];

                                // If we have a prediction for the same product which expires later than current day.
                                if ($predictionDate >= $date) {
                                    $extraPeriod = (int)((strtotime($predictionDate) - strtotime($date)) / 86400);
                                    $extendPeriod += $extraPeriod;
                                }

                                $newDateTime = strtotime($date . '+' . $extendPeriod . 'days');
                                $predictionsInfo[$productId][$diopter] = date('Y-m-d', $newDateTime);
                            }
                        }
                    }
                }
            }

            return $predictionsInfo;
        }

        return array();
    }
}