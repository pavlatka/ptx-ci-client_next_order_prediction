<?php
header('content-type: text/plain; charset=utf8');
require_once './vendor/autoload.php';

$customerObj = new Customer();

for($i = 1; $i < 5; $i++) {
    echo 'Customer ' . $i . "\r\n";
    echo str_repeat('_', 10) . "\r\n";
    print_r($customerObj->predictNextPurchase($i));
    echo "\r\n";
}